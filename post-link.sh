if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then

  # extract CUDA version from package name so
  # we know what the executable is called
  CUDA_VER=${PKG_NAME/fsl-eddy-cuda-/}
  ${FSLDIR}/share/fsl/sbin/createFSLWrapper eddy eddy_cuda${CUDA_VER}
fi
