if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
  CUDA_VER=${PKG_NAME/fsl-eddy-cuda-/}
  ${FSLDIR}/share/fsl/sbin/removeFSLWrapper eddy eddy_cuda${CUDA_VER}
fi
