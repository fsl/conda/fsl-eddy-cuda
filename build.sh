#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# Only build/install GPU components,
# static linking to CUDA runtime
make CUDA_STATIC=1 cuda=1
make CUDA_STATIC=1 cuda=1 install
